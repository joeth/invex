# invex

Invidious URL grabber

invex is a simple CLI tool which lets you search for YouTube videos through Invidious. The result gives you a URL which you can use for anything -- you can paste the URL to mpv, youtube-dl, or anywhere else you'd like. Gone are the days where you have to open up a browser and jump through menus and loading times and thumbnails with silly faces.

### Usage

Don't be a baby and run this in the terminal:

    git clone https://gitlab.com/steelinferno/invex.git
    cd invex
    python invex.py
    
type in what you want to search, highlight and copy the URL that you want.

![cli preview](png.png)

### Known issues

st (simple terminal) crashes if you search for "best pirate car".
tty works fine. Needs more testing.

### Further plans

- Select link with a number or something
- Have it automatically send the URL to your clipboard
- show video length, views
- preview thumbnail for selected link
- add option to send it straight to mpv or youtube-dl
- make it installable as a package, runnable through bash
- get a license

This is the initial release, it 'just works'. I plan on adding a lot more features, but my focus is on keeping it somewhat minimal. Unlike mps-youtube which takes like 5 seconds to turn on, I want this thing to be instant. Also, it should use Invidio.us links instead of youtube links. I don't want to make/use a google account just to use the youtube api.
