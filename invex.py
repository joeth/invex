#!/usr/bin/env python3

# imports and title

import argparse
import os
import requests
url = 'https://invidio.us/'
print("Invex - Invidio.us URL grabber")

''' Connection tester. Disabled by default to speed up script.
res = requests.get(url)
print(Style.DIM + 'Connected successfully...') if (res.status_code == 200
        ) else print(Fore.RED + 'GET status code: ' + str(res.status_code))
if (res.status_code != 200) : quit()
'''
def setPlayer():
    configDir=(os.getenv("HOME")+"/.config/invex.conf")
    config=open(configDir, "r+")
    if config.readline() == "":
        print("Config file not found... defaulting to MPV.")
        print("Change this in ~/.config/invex.conf")
        config.write("PLAYER = MPV\n")
    elif config.read().contains("PLAYER = MPV"):
        player=MPV
        print("player selected as MPV")
    elif config.read().contains("PLAYER = VLC"):
        player=VLC
        print("player selected as VLC")
    else:
        print("unfortunately this only supports MPV and VLC")
        print("Defaulting to MPV")
        player=MPV

    config.close()

# parsing arguements
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--search", dest="search", help="Search Query")
#parser.add_argument("-c", "--copy", dest="copy", help="Copy link to clipboard")
#parser.add_arguement("-o", "--only-search" dest="onlysearch" help="Only search (don't open)")
global args = parser.parse_args()


if not args.search:
    # search
    print('\nInput search query:')
    args.search = input('>').replace(' ','+')

isq = args.search
sq = url+'search?q='+isq
print(sq)
def onlySearch():
# invidious api usage
api = url+'api/v1/search?q='+isq
j = requests.get(api).json()
for i in j:
    print('URL: ' 
            'https://invidio.us/watch?v=%s' % (i['videoId'])
            + ' - Title: %s' % (i['title']))



''' API URL Provider for debugging
print(Fore.CYAN + 'API: ' + api)
print(Style.RESET_ALL)
'''
